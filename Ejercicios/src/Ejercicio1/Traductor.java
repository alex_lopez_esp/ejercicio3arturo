package Ejercicio1;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Traductor {
    private static final Map<String, String> DICCIONARIO = new HashMap<String, String>();
    private static final Scanner teclado = new Scanner(System.in);


    public static void main(String[] args) {
        ejecutar(args);

    }

    private static void ejecutar(String[] argumentos) {
        BufferedReader br;

        File file=new File("Translations.txt");
        try {
            FileOutputStream fos=new FileOutputStream(file,false);
            DataOutputStream dos=new DataOutputStream(fos);

            if (argumentos.length > 0 && comprobar(argumentos)) {
                try {
                    if (argumentos[1].equals("-d")){
                        br=new BufferedReader(new FileReader(argumentos[0]));
                    }else {
                        br=new BufferedReader(new FileReader(argumentos[1]));
                    }

                    String linea;
                    String[] palabros;
                    while ((linea = br.readLine()) != null) {
                        palabros = linea.split(" ");
                        DICCIONARIO.put(palabros[0], palabros[1]);

                    }
                    while (!(linea = teclado.nextLine()).equals("fin")) {
                        dos.writeUTF(linea+" - >> "+DICCIONARIO.get(linea)+"\n");
                        System.out.println(DICCIONARIO.getOrDefault(linea, "Desconicido"));

                    }
                    System.out.println(linea);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else {
                diccionarioInterno();
                String linea = teclado.nextLine().toLowerCase();
                while (!linea.equals("fin")) {
                    if (DICCIONARIO.containsKey(linea)) {
                        System.out.println(DICCIONARIO.get(linea));
                        dos.writeUTF(linea+" - >> "+DICCIONARIO.get(linea)+"\n");
                        linea = teclado.nextLine().toLowerCase();
                    } else {
                        System.out.println("Desconicido");
                        linea = teclado.nextLine().toLowerCase();
                    }
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static boolean comprobar(String[] args){
        boolean comprobante=false;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-d")){
                comprobante=true;
            }

        }
        return comprobante;
    }


    private static void diccionarioInterno() {
        DICCIONARIO.put("us", "nosotros");
        DICCIONARIO.put("most", "la mayoria");
        DICCIONARIO.put("day", "día");
        DICCIONARIO.put("give", "dat");
        DICCIONARIO.put("these", "estos , estas");
        DICCIONARIO.put("any", " alguno/a, cualquier/a");
        DICCIONARIO.put("because", "porque");
        DICCIONARIO.put("want", "querer");
        DICCIONARIO.put("new", "nuevo");
        DICCIONARIO.put("even", "incluso, aún, parejo");
        DICCIONARIO.put("way", "camino, manera, método");
        DICCIONARIO.put("well", "bien");
        DICCIONARIO.put("first", "primero/a");
        DICCIONARIO.put("work", "trabajo, trabajar");
        DICCIONARIO.put("our", "nuestro/a");
        DICCIONARIO.put("how", "cómo, como");
        DICCIONARIO.put("two", "dos");
        DICCIONARIO.put("use", "usar, uso");
        DICCIONARIO.put("after", "después de");
        DICCIONARIO.put("back", "de vuelta, atrás, espalda,");
        DICCIONARIO.put("also", "también");
        DICCIONARIO.put("think", "pensar");
        DICCIONARIO.put("over", "encima de, por encima de, más de");
        DICCIONARIO.put("its", "su");
        DICCIONARIO.put("come", "venir, llegar");
        DICCIONARIO.put("only", "solo, solamente, únicamente");
        DICCIONARIO.put("look", "mirar, buscar, parecer");
        DICCIONARIO.put("now", "ahora");
        DICCIONARIO.put("then", "entonces");
        DICCIONARIO.put("than", "que");
        DICCIONARIO.put("other", "otra/o");
        DICCIONARIO.put("see", "ver");
        DICCIONARIO.put("them", "ellos, los (posesivo)");
        DICCIONARIO.put("could", "podría, podríamos, podrían");
        DICCIONARIO.put("some", "algo, alguno, algunas");
        DICCIONARIO.put("good", "bueno");
        DICCIONARIO.put("your", "tu/tus (posesivo)");
        DICCIONARIO.put("year", "año");
        DICCIONARIO.put("into", "dentro de, en, contra");
        DICCIONARIO.put("people", "gente, personas");
        DICCIONARIO.put("take", "tomar");

    }
}
