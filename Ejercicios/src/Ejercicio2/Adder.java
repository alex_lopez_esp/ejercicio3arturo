package Ejercicio2;

import java.io.*;



public class Adder {
    public static void main(String[] args) {
        File fichero = new File(args[0]);

        int calculo = 0;
        BufferedReader br ;
        try {
            br = new BufferedReader(new FileReader(fichero));
            String linea;
            while ((linea = br.readLine()) != null) {
                calculo += Integer.parseInt(linea);
            }
            System.out.println(calculo);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

