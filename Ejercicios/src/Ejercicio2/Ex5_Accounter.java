package Ejercicio2;

import java.io.*;
import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;

public class Ex5_Accounter {
    public static void main(String[] args) {
        String comando="java -jar C:\\Users\\a2000\\Desktop\\Ejercicioarturito\\out\\artifacts\\Ejercicioarturito_jar2\\Ejercicioarturito.jar";
        List<String> lineas=new ArrayList<>();
        BufferedReader br;

        String contenido;
        int totalCalculo = 0;
        for (int i = 0; i < args.length; i++) {
            lineas.add(args[i]);
        }

        List<String> concatenar;
        for (int i = 0; i < lineas.size(); i++) {
           concatenar= new ArrayList<>(Arrays.asList(comando.split(" ")));
           concatenar.add(lineas.get(i));
            try {

                ProcessBuilder pb=new ProcessBuilder(concatenar);
                Process process=pb.start();
                process.getInputStream();
                InputStream inputStream=process.getInputStream();
                br=new BufferedReader(new InputStreamReader(inputStream));
                contenido=br.readLine();
                totalCalculo+=Integer.parseInt(contenido);
                escribirEnDocumento(String.valueOf(totalCalculo));

            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(totalCalculo);

        }

    }
    private static void escribirEnDocumento(String total){

        try {
            BufferedWriter br =new BufferedWriter(new FileWriter("Total.txt"));
            br.write("El total del calculo es :"+total);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
